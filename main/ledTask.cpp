#include "WS2812.h"
#include <freertos/task.h>
#include <stdlib.h>
#include <string.h>
#include <esp_log.h>
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "ledTask.h"
const static char* TAG="ledTask";
class LedTask{
    xSemaphoreHandle _lock;
    void lockInit(){
        vSemaphoreCreateBinary(_lock);
    }
    void lock(){
        xSemaphoreTake(_lock,portMAX_DELAY);
    }
    void unlock(){
        xSemaphoreGive(_lock);
    }


    WS2812 *ws;
    uint16_t diff_ms=0;
    uint16_t offset_ms=200;
    uint16_t maxDelay_ms=300;
    
    uint8_t oldIdx=0;
    uint16_t pixelCount;
    const static  uint8_t nofBuf=10;
    uint16_t play_ms[nofBuf];
    uint8_t play_idx[nofBuf];
    uint8_t arrive[nofBuf];
    
    uint8_t inPos;
    uint8_t outPos;
    uint8_t *pixBuf;
    public:
    LedTask(){
        offset_ms=200;
        maxDelay_ms=300;
        ws=NULL;
        inPos=0;
        outPos=0;
        _lock=NULL;
        pixBuf=NULL;
        lockInit();
    }
    ~LedTask(){
        delete ws;
        if(pixBuf)free(pixBuf);
        pixBuf=NULL;
        ws=NULL;
    }
    void task(void){
        const portTickType span=16/portTICK_PERIOD_MS;//62.5Hz
        portTickType now=xTaskGetTickCount();
        bool pixfilled=false;
        while(true){
            vTaskDelayUntil( &now, span );
            lock();
            uint8_t _inPos=inPos;
            uint16_t _diff_ms=diff_ms;
            unlock();

            if(_inPos==outPos){
                continue; //no data
            }
            #if 1
            if(!pixfilled){
                
                uint8_t *p =pixBuf+pixelCount*3*outPos;
                #if 1 //for Release
                ws->setPixels(1,pixelCount,p);
                #else  //for Debug
                int onCount=0;           
                for(int i=0;i<pixelCount;i++){
                    if(p[0] || p[1] || p[2])onCount++;
                    ws->setPixel(i+1,p[0],p[1],p[2]);
                    p+=3;
                }
                ESP_LOGI(TAG,"OnCount:%d",onCount);
                #endif
                pixfilled=true;
            }
            #endif
            //now_data_ms is the time to play.
            uint16_t now_data_ms = (uint16_t) (xTaskGetTickCount()*portTICK_PERIOD_MS)+_diff_ms-offset_ms;
            if( (int16_t)(play_ms[outPos]-now_data_ms) <0 ){ //It's time to play
                static uint8_t oldIdx=0;
                if(play_idx[outPos]-oldIdx!=1){
                    ESP_LOGE(TAG,"Skip IDX:%d -> %d",oldIdx,play_idx[outPos]);
                }
                oldIdx=play_idx[outPos];
                if(arrive[outPos]==0xff){
                    ws->show();
                }else{
                    ESP_LOGE(TAG,"Skip Not filled data:%X",arrive[outPos]);
                }
                pixfilled=false;
                lock();
                outPos++;outPos%=nofBuf;
                unlock();
                if(outPos==_inPos){
                    ESP_LOGE(TAG,"buffer empty");
                }
            }
        }
    }

    //data format
    //------header 6byte 
    //uint8_t idx 
    //time uint16_t (ms)
    //start led pos uint8_t (startPos/8)   // 0,400/8=50,800/8=100 wii be used
    //data len uint8_t (N/2)               // 200,200,225 will used
    //-----data 
    //leddata rgb  uint8_t[N*3] (0..255)
    void command(uint8_t *data,int dataLen){
        const uint8_t idx=data[0];
        const uint16_t data_ms= (uint16_t)(data[1])*0x100 + data[2];
        const uint8_t startPos_q=data[3];
        const uint8_t len_h=data[4];
        if(len_h && (5+len_h*2*3!=dataLen) ){
            ESP_LOGE(TAG,"Data length error excepted %d but %d",5+len_h*2*3,dataLen);
            return;
        }
        //ESP_LOGI(TAG,"cmd: %03d %03d %03d",idx,startPos_q,len_h);
        lock();
        uint16_t now_ms= (uint16_t) (xTaskGetTickCount() *portTICK_PERIOD_MS) +diff_ms;
        uint16_t d=now_ms-data_ms;
        //Ex. WiFi latency is 10-50ms. in case of 10ms d is zero, in case of 50ms d is 40.
        if(d>maxDelay_ms){ //set diff_ms when over maxDelay_ms or minus ms. 
            if((int16_t)d <-3 ||(int16_t)d >offset_ms){
                ESP_LOGI(TAG,"correct %d ",(int16_t)d);
            }
            diff_ms-=d; 
        }
        if(idx!=oldIdx){//new Data  CAUTION: data is orderd in a same segment.
            diff_ms-=2; //little bit faster timer. 
            if( (inPos+1)%nofBuf==outPos ){
                outPos=0;
                inPos=0;
                diff_ms-=d;
                unlock();
                ESP_LOGE(TAG,"buffer over run  do reset");
                return ;
            }
            inPos++;inPos%=nofBuf;
            play_ms[inPos]=data_ms;
            play_idx[inPos]=idx;
            arrive[inPos]=0;
            oldIdx=idx;
        }
        unlock();
        #if 0
        {
            portTickType now=xTaskGetTickCount();
            uint16_t now_data_ms = (uint16_t) (now*portTICK_PERIOD_MS)+diff_ms;
            ESP_LOGI(TAG,"MS data - now = %d ",now_data_ms - data_ms);

        }
        #endif
        //fill data
        if(len_h){
            uint16_t startPos=startPos_q*8;
            uint16_t len=((size_t)len_h)*2;
            uint8_t *p = pixBuf+(inPos*pixelCount+startPos)*3;
            memcpy(p,data+5,len*3);
            uint8_t mask=1;
            for(int i=0;i<8;i++){
                int p=i*pixelCount/8+pixelCount/16;
                if(startPos<=p&& p<startPos+len)arrive[inPos]|=mask;
                mask<<=1;
            }

        }else{//command mode
            switch(startPos_q){
                case 0://set offset_ms
                        offset_ms=(uint16_t)(data[5])*0x100+data[6];
                        ESP_LOGI(TAG,"offset_ms: %d",offset_ms);
                    break;
                case 1://set maxDelay_ms
                        maxDelay_ms=(uint16_t)(data[5])*0x100+data[6];
                        ESP_LOGI(TAG,"maxDelay_ms: %d",maxDelay_ms);
                    break;
                default:
                    break;

            }

        }
    }
    void setup(gpio_num_t gpioNum, uint16_t _pixelCount, int channel/* RMT_CHANNEL_0*/){
        pixelCount=_pixelCount;
        ESP_LOGI(TAG, "Free heap1: %u  and lagest is %d", xPortGetFreeHeapSize(),heap_caps_get_largest_free_block(MALLOC_CAP_32BIT ) );
        ws=new WS2812( gpioNum,  pixelCount+1, channel);
        assert(ws);
        ESP_LOGI(TAG, "Free heap2: %u  and lagest is %d", xPortGetFreeHeapSize(),heap_caps_get_largest_free_block(MALLOC_CAP_32BIT ) );
        pixBuf=(uint8_t*)calloc(pixelCount*3*nofBuf,1);
        assert(pixBuf);
        ESP_LOGI(TAG, "Free heap3: %u  and lagest is %d", xPortGetFreeHeapSize(),heap_caps_get_largest_free_block(MALLOC_CAP_32BIT ) );

        ws->setPixel(0,0x000000);
        //initial is darkRed 
        for (int i=0;i<pixelCount;i++){
            uint8_t r=i%0x0f;
            uint8_t g=(i>>4)%0x0f;
            uint8_t b=(i>>8)%0x0f;

            ws->setPixel(i+1,r,g,b);
        }
        ws->show();
    }
    
};


extern "C"{
    LedTask *ledTask;
    static void _task(void * dummy){
        ledTask->task();
    }
    void begin_led_task()
    {
        ledTask=new LedTask();
        ledTask->setup(GPIO_NUM_27,NOF_LED,RMT_CHANNEL_0);
        xTaskCreatePinnedToCore(_task,"ledtask",2048,NULL,10,NULL,1);
    }
    void led_command(uint8_t *data,int dataLen){
        ledTask->command(data,dataLen);
    }

}