# -*- coding: utf-8 -*-
"""
Created on Tue Aug 28 22:38:25 2018

@author: akira
"""
import setting
import cv2
CV_CAP_PROP_POS_FRAMES = 1
import numpy as np
import pickle
DoEnhance=False
ReadOutLen=100
level_thresh_override=00



nofOnLedL=7
nofOnLed=2**nofOnLedL 
port = 13531

def setHost(_host,_nofLed):
    global nofLed,nofOnLedL,nofOnLED,nofBlock,nofLedR,host
    host=_host
    nofLed=_nofLed
    nofBlock=(nofLed+nofOnLed-1)//nofOnLed;
    nofLedR=nofBlock*nofOnLed;



def getMasks():
    masks=[]
    for nowBlock in range(nofBlock):
        for n in range(nofOnLedL):
            for b in range(2): #get low bit or high bit
                mask=[]
                for i in range(nofOnLed):
                    mod=2**n
                    o=1
                    if(mod&i): o^=1
                    o^=b
                    if(o!=0):mask.append(i)
                masks.append( np.array(mask)*nofBlock+nowBlock)
    return masks



cap = cv2.VideoCapture("data.mov")
level=[]
print("check signal level")
#read out
cap.set(CV_CAP_PROP_POS_FRAMES,ReadOutLen)

    
while(True):
    ret, frame = cap.read()
    if(not ret):break
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY).astype(np.float32)
    t=128
    if(DoEnhance):
        gaussian_1 = cv2.GaussianBlur(gray, (5,5), 0)
        gaussian_3 = cv2.GaussianBlur(gray, (25,25), 0)
        unsharp_image = gaussian_1-gaussian_3 #cv2.addWeighted(gray, 1.5, gaussian_3, -0.5, 0, gray)
        gray=unsharp_image
        t=40
    level.append(np.count_nonzero(gray>t))
    #cv2.imshow("in",gray)
level=np.array(level)


##########################################################
#########setting threshold
##########################################################
level_thresh=(np.percentile(level,70) +np.percentile(level,40))/2
if(level_thresh_override>0):
    level_thresh=level_thresh_override ##########################overide



on=level>level_thresh
on=on[1:]*on[:-1] #erode
off=level<=level_thresh
off=off[1:]*off[:-1] #erode
assert off[0],"start must be dark"
assert off[-1],"end must be dark"
assert (on*off).any() == False, "dark and light must not be in same"
#cut lead in /out
#lpos[0] is 1st "not Off" position
#lpos[1] is last "not Off" position
lpos=np.nonzero(off==False)[0][np.array((0,-1))]
on=on[:lpos[1]+4]
off=off[:lpos[1]+4]

offset=lpos[0]-5
on=on[offset:]
off=off[offset:]
off[-1]=0
on[-1]=0

cap.set(CV_CAP_PROP_POS_FRAMES,ReadOutLen+offset)


frames=[] #off,on,....,off
count=0
nowFrame=None
mode=0 #-1:off 0:None 1:on
print("read frames")

for i in range(len(on)):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY).astype(np.float)/255
    if mode==-1:  #off is end
        if(off[i]==0):
            frames.append(nowFrame/count)
            print("Off",count)
            mode=0
    elif mode==1:
        if(on[i]==0):
            frames.append(nowFrame/count)
            print("On",count)
            mode=0
    else:
        nowFrame=None
        count=0
        if(on[i]): mode=1
        if(off[i]): mode=-1
        
    if(mode!=0):     
        if(nowFrame is None):
            nowFrame=gray
            count=1
        else:
            nowFrame+=gray
            count+=1


if(False):
    for f in frames:
        cv2.imshow("frame",f)
        cv2.waitKey(2)


     
dframes=[]
for i in range(len(frames)//2):
    dframes.append( np.maximum(0, (frames[2*i+1]-0.5*(frames[2*i]+frames[2*i+2]))  ))            

dframes/=np.max(dframes)
if(True):
    for df in dframes:
        cv2.imshow("dframe",df)
        cv2.waitKey(2)

maskss=[]
total_masklen=0
for h,l in zip(setting.hosts,setting.nofLeds):
    setHost(h,l)
    maskss.append(getMasks())
    total_masklen+=len(maskss[-1])

assert  total_masklen==len(dframes*255), "frames dose not match"

def getSingle(idx,dframes,masks):
    t=[i for i,a in  enumerate(masks) if idx in a]
    ret=dframes[t[0]]+0
    for i in range(1,len(t)):
        ret*=dframes[t[i]]    
    ret=ret**(2/len(t))
    
    
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(ret)
    show=ret/np.max(ret)
    cv2.line(show,(0,max_loc[1]),(show.shape[1],max_loc[1]) ,(0.5) )
    cv2.line(show,(max_loc[0],0),(max_loc[0],show.shape[0]) ,(0.5) )
    cv2.imshow("mux",show)
    print("ret max min" ,np.max(ret),np.min(ret))
    cv2.waitKey(1)
    return ret,max_loc,max_val





levelss=[]
locss=[]
now=0
count=0
for h,masks in zip(setting.hosts,maskss):
    mask_len=len(maskss[count])
    print(h,mask_len )
    setHost(h,l)
    levels=[]
    locs=[]
    for i in range(nofLed):
        a,loc,maxv=getSingle(i,dframes[now:now+mask_len],masks)
        levels.append(maxv)
        locs.append(loc)

    locss.append(np.array(locs))
    levelss.append(np.array(levels))
    now+=mask_len
    count+=1

with open('pos.pickle', mode='wb') as f:
    pickle.dump({"pos":locss,"level":levelss}, f)

a=np.sort(levelss).transpose()



