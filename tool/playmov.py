# -*- coding: utf-8 -*-
"""
Created on Sun Sep  2 07:16:46 2018

@author: akira
"""
file="badapple20.mp4"
import numpy as np
import time
import socket
import cv2
import setting
import pickle
CV_CAP_PROP_POS_FRAMES = 1

port = 13531
timeSpan=1/20.0
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def send_command(host,data,idx,commandNo):
    global sock,port
    d=bytearray(5+len(data))
    d[0]=idx
    ms=round(time.time()*1000) % 65536
    d[1]=ms//256
    d[2]=ms%256
    d[3]=commandNo
    d[4]=0
    for i in range(len(data)):
        d[5+i]=data[i]
    sock.sendto(d, (host, port))
    
def set_offset(host,offset):
    send_command(host,[offset//256,offset%256],0,0)

def set_maxdelay(host,d):
    send_command(host,[d//256,d%256],0,0)

def _send(host,data,idx,startPos_Q):
    global port,sock
    assert data.shape[0]%2==0, "data len must be even"
    d=bytearray(5+3*data.shape[0])
    d[0]=idx
    ms=round(time.time()*1000) % 65536
    d[1]=ms//256
    d[2]=ms%256
    d[3]=startPos_Q
    d[4]=data.shape[0]//2
    for i in range(data.shape[0]):
        d[5+i*3+2]=data[i,0] #B
        d[5+i*3+0]=data[i,1] #G
        d[5+i*3+1]=data[i,2] #red
    #GRB  BGR
    sock.sendto(d, (host, port))

def send(host,data):
    global sock
    if not hasattr(send, "idx"):send.idx=0
    data_len=data.shape[0]
    chunk_max=((1472-6)//3//8)*8
    for j in range(2):  #dup send to aboid packet loss
        for i in range(0,data_len,chunk_max):
            chunk_len=min((chunk_max,data_len-i))
            _send(host,data[i:i+chunk_len,:],send.idx,i//8)
    send.idx+=1
    send.idx%=256



for host in setting.hosts:
    set_offset(host,100)
    set_maxdelay(host,200)
    





f=0
def loop():
    global f
    global hosts
    if not hasattr(loop, "count"):
        loop.count= 0
    loop.count += 1
 
    ret,f=loop.cap.read()
    if(not ret):
        loop.cap.set(CV_CAP_PROP_POS_FRAMES,1)
        ret,f=loop.cap.read()
    cv2.imshow("f",f)
    f=f.transpose((1, 0, 2))
    unitId=0
    for host,nof_led in zip(setting.hosts,setting.nofLeds):
        led=np.zeros( (nof_led,3),dtype=np.ubyte)
        for i in range(nof_led):
            if(not loop.valid[unitId][i]):continue
            pos=((loop.pos[unitId][i,:]-loop.minPos)*loop.xyrate).astype(np.int32)
            v=f[pos[0],pos[1],:]
            led[i,:]=v//8
        send(host,led)
        unitId+=1
    cv2.waitKey(1)
    
    
def setup():
    with open('pos.pickle', mode='rb') as f:
        d=pickle.load(f)
    loop.pos=d["pos"]
    loop.valid=[ a>0.00000000000006 for a in d["level"]]
    print("nof valid LED",np.count_nonzero(loop.valid))
    loop.maxPos=np.array((0,0)) 
    loop.minPos=np.array((1e+10,1e+10)) 

    for i in range(len(loop.pos)):
        lmin=loop.pos[i][loop.valid[i]].min(axis=0)
        lmax=loop.pos[i][loop.valid[i]].max(axis=0)
        loop.maxPos=np.maximum(loop.maxPos,lmax)
        loop.minPos=np.minimum(loop.minPos,lmin)
    loop.xylen=loop.maxPos-loop.minPos


    cap = cv2.VideoCapture(file)
    ret,f=cap.read()
    f=f.transpose((1, 0, 2))
    
    loop.cap=cap
    loop.xyrate=(np.array(f.shape)[:2] - 1)/loop.xylen
    print(f.shape)
        
    
setup() 

start_time=time.time()
next_time=start_time+timeSpan
while(True):
    loop();
    now_time=time.time()
    d=next_time-now_time 
    if(d>0):
        time.sleep(d)
    next_time+=timeSpan