# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 22:50:30 2018

@author: akira
"""

import time
import socket
import numpy as np



hosts = ['192.168.10.31','192.168.10.32','192.168.10.33','192.168.10.34'] #'224.0.0.1'
port = 13531
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
d=None
def _send(sock,data,idx,startPos_Q):
    global d
    global port,hosts
    assert data.shape[0]%2==0, "data len must be even"
    d=bytearray(5+3*data.shape[0])
    d[0]=idx
    ms=round(time.time()*1000) % 65536
    d[1]=ms//256
    d[2]=ms%256
    d[3]=startPos_Q
    d[4]=data.shape[0]//2
    for i in range(data.shape[0]):
        d[5+i*3+0]=data[i,0]
        d[5+i*3+1]=data[i,1]
        d[5+i*3+2]=data[i,2]
    
    for host in hosts:
        sock.sendto(d, (host, port))
    print(idx,startPos_Q,data.shape[0]//2,ms/1000.0)
    
    

def send(sock,data):
    if not hasattr(send, "idx"):send.idx=0
    data_len=data.shape[0]
    chunk_max=((1472-6)//3//8)*8
    for j in range(1):  #dup send to aboid packet loss
        for i in range(0,data_len,chunk_max):
            chunk_len=min((chunk_max,data_len-i))
            _send(sock,data[i:i+chunk_len,:],send.idx,i//8)
    send.idx+=1
    send.idx%=256

count=0
loopLen=1250
while True:
    print(count % loopLen)
    led=np.zeros( (1250,3),dtype=np.ubyte)
    #led[count % loopLen,0]=255; #G
    #led[ (count+1)% loopLen,1]=255;#R
    for j in range(100):
        led[ (count+2+j)%loopLen,2]=255-j; #B
    send(sock,led)
    count += 30
    time.sleep(1.0/20)