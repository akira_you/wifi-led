# -*- coding: utf-8 -*-
"""
pos to calibed pos
Created on Wed Oct  3 23:03:48 2018

@author: akira
"""
#To serve the data run python -m http.server 8000 

import numpy as np
import pickle
import cv2
import matplotlib.pyplot as plt
import math
from pyquaternion import Quaternion
from mpl_toolkits.mplot3d import axes3d


ValidThresh=0.1*0.3
Height=2.6 #height is z
RotQ=Quaternion(axis=[1.0, -0.0, 0], angle=0.65) ## angle 0　means Down







camera_mat = np.loadtxt('K.csv', delimiter=',')
dist_coef = np.loadtxt('d.csv', delimiter=',')

if(False):
    poss=[]
    poss.append(np.r_[np.meshgrid(np.linspace(0,1279,20),np.linspace(0,720,20)  )].transpose().reshape(-1,2))
    
    valid=[]
    valid.append([True]*len(poss[0]))  
else:
    with open('pos.pickle', mode='rb') as f:
        d=pickle.load(f)
    poss=d["pos"]
    valid=[ a>ValidThresh for a in d["level"]]



tans=[]
for i in range(len(poss)):
    tans.append(cv2.undistortPoints(np.array([poss[i]]).astype(np.float),camera_mat,dist_coef)[0])

plt.figure("poss")
plt.clf()
for i in range(len(tans)):
    plt.plot(poss[i][valid[i],0],poss[i][valid[i],1],"o")
    

plt.figure("tans")
plt.clf()
for i in range(len(tans)):
    plt.plot(180/math.pi*np.arctan(tans[i][valid[i],0]),180/math.pi*np.arctan(tans[i][valid[i],1]),"o")
    


def doRot(t):
    global Height,RotQ
    t=np.array( (t[0],t[1],1.0))
    t=RotQ.rotate(t)
    t*=Height/t[2]
    return t
def doRotR(t):
    global Height,RotQ
    t=np.array( (t[0],t[1],1))
    t=RotQ.rotate(t)
    return t
    

cp=[]
t=None
for i in range(len(tans)):
    cp.append([])
    for j in range(tans[i].shape[0]):
        cp[-1].append(doRot(tans[i][j,:]))
    cp[-1]=np.array(cp[-1])

plt.figure("caribpos")
plt.clf()


for i in range(len(cp)):
    plt.plot(cp[i][valid[i],0],cp[i][valid[i],1],"o")
    
plt.axes().set_aspect('equal', 'datalim')




fig = plt.figure("3D")
plt.clf()
ax = fig.add_subplot(111, projection='3d')
ax.set_aspect('equal', 'datalim')



D=np.array(([0,0,0], doRotR([0,0]))).transpose()
ax.plot(D[0],D[1],D[2]*-1,'k-')

wd=math.tan(55*math.pi/180)
hd=math.tan(35*math.pi/180)
D=np.array((doRotR([0,0]) ,doRotR([wd,hd]) ,doRotR([-wd,hd]),doRotR([-wd,-hd]),doRotR([wd,-hd]) ,doRotR([wd,hd]))  ).transpose()
ax.plot(D[0],D[1],D[2]*-1,'r-')

ax.set_xlim3d([-6,6])
ax.set_ylim3d([-6,6])
ax.set_zlim3d([-6,6])

D=np.array((doRot([wd,hd]) ,[0,0,0] ,doRot([-wd,hd]),[0,0,0],doRot([-wd,-hd]),[0,0,0],doRot([wd,-hd]) ,[0,0,0],doRot([wd,hd])          )  ).transpose()
ax.plot(D[0],D[1],D[2]*-1,'k-')
D=np.array((doRot([wd,hd]) ,doRot([-wd,hd]),doRot([-wd,-hd]),doRot([wd,-hd]) ,doRot([wd,hd]))  ).transpose()
ax.plot(D[0],D[1],D[2]*-1,'k-')


for i in range(len(cp)):
    ax.plot(cp[i][valid[i],0],cp[i][valid[i],1],cp[i][valid[i],2]*-1,".")




    
import setting
import json
with open("pos.json","w") as fp:
    json.dump( {"level":[a.tolist() for a in  d["level"]],"pos":[ a.tolist() for a in cp],"host":setting.hosts},fp)
    