# -*- coding: utf-8 -*-
"""
Created on Sun Sep  2 07:16:46 2018

@author: akira
"""

import numpy as np
import time
import socket

hosts = ['192.168.1.214']
nof_led=[1250]
port = 13531
timeSpan=1/15.0
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def send_command(host,data,idx,commandNo):
    global sock,port
    d=bytearray(5+len(data))
    d[0]=idx
    ms=round(time.time()*1000) % 65536
    d[1]=ms//256
    d[2]=ms%256
    d[3]=commandNo
    d[4]=0
    for i in range(len(data)):
        d[5+i]=data[i]
    sock.sendto(d, (host, port))
    
def set_offset(host,offset):
    send_command(host,[offset//256,offset%256],0,0)

def set_maxdelay(host,d):
    send_command(host,[d//256,d%256],0,0)

def _send(host,data,idx,startPos_Q):
    global port,sock
    assert data.shape[0]%2==0, "data len must be even"
    d=bytearray(5+3*data.shape[0])
    d[0]=idx
    ms=round(time.time()*1000) % 65536
    d[1]=ms//256
    d[2]=ms%256
    d[3]=startPos_Q
    d[4]=data.shape[0]//2
    for i in range(data.shape[0]):
        d[5+i*3+0]=data[i,0]
        d[5+i*3+1]=data[i,1]
        d[5+i*3+2]=data[i,2]
    
    sock.sendto(d, (host, port))

def send(host,data):
    global sock
    if not hasattr(send, "idx"):send.idx=0
    data_len=data.shape[0]
    chunk_max=((1472-6)//3//8)*8
    for j in range(2):  #dup send to aboid packet loss
        for i in range(0,data_len,chunk_max):
            chunk_len=min((chunk_max,data_len-i))
            _send(host,data[i:i+chunk_len,:],send.idx,i//8)
    send.idx+=1
    send.idx%=256



for host in hosts:
    set_offset(host,100)
    set_maxdelay(host,200)
    






def loop():
    global hosts
    step=10
    if not hasattr(loop, "count"):
        loop.count= 0
        loop.xy=np.zeros(2)
    loop.count += 1
    loop.xy+=step//2
    loop.xy %= loop.xylen
    xy=loop.xy+loop.minPos
    unitId=0
    hit=0
    led=np.zeros( (nof_led[unitId],3),dtype=np.ubyte)
    for i in range(nof_led[unitId]):
        if(not loop.valid[i]):continue
        if(xy[0]//step==loop.pos[i,0]//step):
            led[i][0]=10
            hit+=1
        if(xy[1]//step==loop.pos[i,1]//step):
            led[i][1]=10
            hit+=1
    #print(xy,hit)
    send(hosts[unitId],led)
            
    
    
def setup():
    d=np.load("pos.npz")
    loop.pos=d["pos"]
    loop.valid=d["level"]>0.6
    loop.maxPos=loop.pos[loop.valid].max(axis=0)
    loop.minPos=loop.pos[loop.valid].min(axis=0)
    loop.xylen=loop.maxPos-loop.minPos
    
setup() 

start_time=time.time()
while(True):
    loop();
    now_time=time.time()
    d=start_time+timeSpan-now_time 
    if(d>0):
        time.sleep( d)
    start_time=now_time
