# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 22:50:30 2018

@author: akira
"""

import time
import socket
import numpy as np
import setting

nofOnLedL=7
nofOnLed=2**nofOnLedL 
port = 13531

def setHost(_host,_nofLed):
    global nofLed,nofOnLedL,nofOnLED,nofBlock,nofLedR,host
    host=_host
    nofLed=_nofLed
    nofBlock=(nofLed+nofOnLed-1)//nofOnLed;
    nofLedR=nofBlock*nofOnLed;

def send_command(data,idx,commandNo):
    global port,host,sock
    d=bytearray(5+len(data))
    d[0]=idx
    ms=round(time.time()*1000) % 65536
    d[1]=ms//256
    d[2]=ms%256
    d[3]=commandNo
    d[4]=0
    for i in range(len(data)):
        d[5+i]=data[i]
    sock.sendto(d, (host, port))
    
def set_offset(offset):
    send_command([offset//256,offset%256],0,0)

def set_maxdelay(d):
    send_command([d//256,d%256],0,0)
    
def _send(data,idx,startPos_Q):
    global d
    global port,host,sock
    assert data.shape[0]%2==0, "data len must be even"
    d=bytearray(5+3*data.shape[0])
    d[0]=idx
    ms=round(time.time()*1000) % 65536
    d[1]=ms//256
    d[2]=ms%256
    d[3]=startPos_Q
    d[4]=data.shape[0]//2
    for i in range(data.shape[0]):
        d[5+i*3+0]=data[i,0]
        d[5+i*3+1]=data[i,1]
        d[5+i*3+2]=data[i,2]
    
    sock.sendto(d, (host, port))
    #print(idx,startPos_Q,data.shape[0]//2,ms/1000.0)

def send(data):
    if not hasattr(send, "idx"):send.idx=0
    data_len=data.shape[0]
    chunk_max=((1472-6)//3//8)*8
    for j in range(2):  #dup send to aboid packet loss
        for i in range(0,data_len,chunk_max):
            chunk_len=min((chunk_max,data_len-i))
            _send(data[i:i+chunk_len,:],send.idx,i//8)        
    send.idx+=1
    send.idx%=256


def show_a_mask(mask):
    global nofLed,nofLedR,nowBlock,nofBlock
    led=np.zeros( (nofLedR,3),dtype=np.ubyte)
    for i in range (3):
        send(led[:nofLed,:])
        time.sleep(1/20) 
    led[nowBlock::nofBlock,:][mask,:]=254
    for i in range (3):
        send(led[:nofLed,:])    
        time.sleep(1/20)



#get n bit pattern
def show_masks():
    global nofOnLedL,nofOnLed
    
    for n in range(nofOnLedL):
        for b in range(2): #get low bit or high bit
            mask=[]
            for i in range(nofOnLed):
                mod=2**n
                o=1
                if(mod&i): o^=1
                o^=b
                if(o!=0):mask.append(i)
            show_a_mask(mask)

def ledOff():
    led=np.zeros( (nofLedR,3),dtype=np.ubyte)
    for i in range (3):
        send(led[:nofLed,:])
        time.sleep(1/20) 



for h,l in zip(setting.hosts,setting.nofLeds):
    setHost(h,l)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    ledData=np.zeros( (nofLedR,3),dtype=np.ubyte)
    set_offset(100)
    set_maxdelay(200)
    #time sync
    for i in range(5):
        ledOff()
        
for h,l in zip(setting.hosts,setting.nofLeds):
    setHost(h,l)
    print("Run",h)
    for i in range(10):
        ledOff()
    
    for nowBlock in range(nofBlock):
        show_masks()
        ledOff()





